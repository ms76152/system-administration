# System Administration for Oracle Cloud Infrastructure (OCI) workloads

[Disclaimer](https://docs.oracle.com/en-us/iaas/Content/legalnotices.htm): The following is intended to outline general system administration support and use the information as a guide. It is intended for information purposes only, and may not be incorporated into any contract. The views expressed on this blog are my own and do not necessarily reflect the views of Oracle.


[Back to Cloud Computing](https://gitlab.com/ms76152/cloud-computing)


## Table of Contents

<!-- TOC depthFrom:1 depthTo:5 withLinks:1 updateOnSave:0 orderedList:0 -->
- Mount a boot volume from one compute instance (or VM) onto another compute instance in order to replace lost ssh keys.
- Set the <a href="#vmtimezone">timezone </a> of your Virtual Machine on Linux to reflect the correct date.
- <a href="#computehealthcheck">Health Check</a> your Virtual machine on OCI.
- A beginner's guide to <a href="#aboutvi">visual editor (vi)</a> in Linux and Unix.
<!-- /TOC -->

## Mount a boot volume from one compute instance (or VM) onto another compute instance in order to replace lost ssh keys

This section will display the process of restoring ssh keys on Oracle Cloud Infrastructure.

For VMs running **Oracle Linux 7.9**, use this [blog](https://blogs.oracle.com/cloud-infrastructure/post/recovering-opc-user-ssh-key-on-oracle-cloud-infrastructure) for detail.

For VMs running **Oracle Linux 8** and higher, use this process below:

What we want to achieve the following: We have lost the ssh keys for the compute instance running Oracle Linux 8.7. Let's call it ol8source. We can no longer ssh to this host _ol8source_.  We will stop ol8source, detach the boot disk from it, attach the boot disk to a running Ununtu server (called _ubuntutarget_). After attaching the boot disk to _ubuntutarget_, we will mount the disk, and replace the public ssh key with a bew key. When this step is complete, we will unmount the disk, re-attach it to _ol8source_, and test the connection with the new key. Thank you [Heinz](https://gitlab.com/hmielimo/next-generation-cloud) for your help.

<img alt="Oracle Linux 8 source, and Ubuntu target" src="images/mount-on-ubuntu.png" width="50%">

Stop Oracle Linux 8.7 compute instance where the ssh keys are lost.

<img alt="Stop Oracle Linux 8.7" src="images/ol8source-stop-instance.png" width="50%" style="float:right">

Detach the boot volume from Oracle Linux 8.7 compute instance.

<img alt="Detach boot" src="images/ol8source-detach-boot.png" width="70%" style="float:right">

Create an Ubuntu compute instance on OCI

<img alt="Ubuntu" src="images/create-ubuntu.png" width="50%" style="float:right">


Connect to the Ubuntu server and attach the disk as a data volume. Attach the boot volume from Oracle Linux 8.7 on Ubuntu host - use the iscsi commands provided by OCI.

<img alt="ssh using private key boot" src="images/ssh.png" width="50%" style="float:right">

Ubuntu OS detail:

<img alt="Ubuntu OS detail" src="images/ubuntutarget-os-detail1.png" width="50%" style="float:right">

Attach block volume:

<img alt="attach" src="images/ubuntutarget-attach-block-vol.png" width="50%" style="float:right">

<img alt="attach" src="images/ubuntutarget-attach-block-vol2.png" width="70%" style="float:right">

<img alt="attach" src="images/ubuntutarget-attach-block-vol3.png" width="50%" style="float:right">



Mount the boot volume as a data disk:

<img alt="attach" src="images/ubuntutarget-os-detail5.png" width="50%" style="float:right">

<img alt="attach" src="images/ubuntutarget-os-detail3.png" width="50%" style="float:right">

<img alt="attach" src="images/ubuntutarget-os-detail6.png" width="50%" style="float:right">



Replace the public key with the new ssh public key in /home/opc/.ssh/authorized_keys

<img alt="vi authorized-keys" src="images/ubuntutarget-os-detail4.png" width="50%" style="float:right">


umount the disk

<img alt="unmount" src="images/ubuntutarget-os-detail7.png" width="50%" style="float:right">


Detach the boot disk from ubuntutarget:

<img alt="Detach" src="images/ubuntutarget-os-detail8.png" width="70%" style="float:right">



Attach the disk back to the original Oracle Linux 8.7 host

<img alt="attach" src="images/ol8source-attach-disk1.png" width="70%" style="float:right">



Test connection : ssh -i private-key opc@ip-address-of-OL8-host

<hr>


<a name="vmtimezone"></a>
## Set the timezone and date of your Linux Virtual Machine on Oracle Cloud Infrastructure
How to change timezone of a VM: [Oracle Doc 2775348.1](https://support.oracle.com/knowledge/Oracle%20Cloud/2775348_1.html)
- $ sudo timedatectl list-timezones
- $ sudo timedatectl set-timezone <desired_timezone>
- example: $ sudo timedatectl set-timezone Africa/Johannesburg
- $ ls -l /etc/localtime
- $ date

<hr>

<a name="aboutvi"></a>
## Beginer's guide to visual editor (vi) on Linux and Unix
vi, or visual is a built-in edit for Unix, and linux.
When the user in single-user mode, vi is sometimes the only editor available to edit config files.
[Link to vi page](https://gitlab.com/command-line1/about-vi)

<hr>

<a name="computehealthcheck"></a>
## Health check for Oracle Cloud Infrastructure (OCI) compute virtual machines

- Make sure you are using the [best practices](https://docs.oracle.com/en/solutions/oci-best-practices/) for Oracle Cloud Infrastructure
- Ensure you are using last instance [generation](https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm)
- Ensure you are using [Flexible shapes](https://docs.oracle.com/en-us/iaas/Content/Compute/References/computeshapes.htm)
- Think about using [Arm-Based Compute](https://docs.oracle.com/en-us/iaas/Content/Compute/References/arm.htm) for web based workloads
- Ensure your instances are distributed across different [Availability Domains and Fault Domains](https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm)
- Ensure your instances are backed up properly
- [Disaster Recovery](https://www.oracle.com/database/technologies/high-availability/oracle-cloud-maa.html) for your workloads - failover to another region or data center
- [High availability](https://www.oracle.com/cloud/public-cloud-regions/#high-availability) for your workloads - failover within the same data center

Tools to use for Oracle Cloud Infrastruture (OCI) Health Check: 
- The [sos command ](https://docs.oracle.com/en/operating-systems/oracle-linux/8/monitoring/monitoring-WorkingWiththesosCommand.html)collects information about a system such as hardware configuration, software configuration, and operational state. You can also use the sos report command to enable diagnostics and analytical functions on the current system. 
- Ready-made health check scripts from [github](https://github.com/SimplyLinuxFAQ/health-check-script) 

[Back to Cloud Computing](https://gitlab.com/ms76152/cloud-computing)



<!--- Links -->
<!-- /Links -->
